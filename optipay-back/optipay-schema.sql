CREATE TABLE client (
  client_id INTEGER PRIMARY KEY AUTO_INCREMENT,
  lastname VARCHAR(50) NOT NULL,
  firstname VARCHAR(50) NOT NULL,
  mail VARCHAR(100) NOT NULL,
  birth_date DATE NOT NULL,
  password VARCHAR(60) NOT NULL
);

CREATE TABLE account (
  account_id INTEGER PRIMARY KEY AUTO_INCREMENT,
  iban VARCHAR(34),
  bank VARCHAR(15) NOT NULL,
  client_id INTEGER NOT NULL,
  account_type VARCHAR(20) NOT NULL,
  bank_account_number VARCHAR(40) NOT NULL,
  FOREIGN KEY(client_id) REFERENCES client(client_id)
);

CREATE TABLE payment (
  payment_id INTEGER PRIMARY KEY AUTO_INCREMENT,
  amount INTEGER NOT NULL,
  client_id INTEGER NOT NULL,
  receiver_iban VARCHAR(34),
  account_id INTEGER NOT NULL,
  payment_date DATE NOT NULL,
  seller_company VARCHAR(30) NOT NULL,
  order_number VARCHAR(20) NOT NULL,
  FOREIGN KEY(client_id) REFERENCES client(client_id),
  FOREIGN KEY(account_id) REFERENCES account(account_id)
);

CREATE TABLE transfer (
  transfer_id INTEGER PRIMARY KEY AUTO_INCREMENT,
  amount INTEGER NOT NULL,
  sender_account_id INTEGER NOT NULL,
  receiver_account_id INTEGER NOT NULL,
  transfer_date DATE NOT NULL,
  payment_id INTEGER NOT NULL,
  FOREIGN KEY(sender_account_id) REFERENCES account(account_id),
  FOREIGN KEY(receiver_account_id) REFERENCES account(account_id),
  FOREIGN KEY(payment_id) REFERENCES payment(payment_id)
);